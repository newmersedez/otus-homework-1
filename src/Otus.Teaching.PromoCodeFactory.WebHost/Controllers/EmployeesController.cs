﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRoleRepository _roleRepository;

        public EmployeesController(
            IEmployeeRepository employeeRepository, 
            IRoleRepository roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetListAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Получение ролей сотрудника
        /// </summary>
        /// <param name="id">Идентификатор сотрудника</param>
        /// <returns>Список ролей сотрудника</returns>
        [HttpGet("{id:guid}/roles")]
        public async Task<ActionResult<IReadOnlyList<RoleItemResponse>>> GetEmployeeRolesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetAsync(id);
            
            if (employee is null) return NotFound();

            var rolesDto = employee.Roles?
                .Select(x => new RoleItemResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                })
                .ToList();

            return Ok(rolesDto);
        }

        /// <summary>
        /// Создание нового сотрудника
        /// </summary>
        /// <returns>Идентификатор нового сотрудника</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync(CreateEmployeeRequest request)
        {
            if (request is null) return BadRequest("Тело запроса обязательно");

            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Roles = (await _roleRepository.GetListAsync(request.RoleIds.Distinct())).ToList()
            };

            _employeeRepository.Add(employee);
            await _employeeRepository.SaveAsync();

            return Ok(employee.Id);
        }

        /// <summary>
        /// Редактирование сотрудника
        /// </summary>
        /// <param name="id">Идентификатор сотрудника</param>
        /// <param name="request">Тело запроса</param>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> EditEmployeeAsync(Guid id, EditEmployeeRequest request)
        {
            var employee = await _employeeRepository.GetAsync(id);

            if (employee is null) return NotFound();

            employee.FirstName = request.FirstName;
            employee.LastName = request.LastName;
            employee.Email = request.Email;
            employee.Roles = (await _roleRepository.GetListAsync(request.RoleIds.Distinct())).ToList();

            await _employeeRepository.SaveAsync();

            return Ok();
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id">Идентификатор сотрудника</param>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetAsync(id);

            if (employee is null) return NotFound();
            
            _employeeRepository.Remove(employee);
            await _employeeRepository.SaveAsync();

            return Ok();
        }
    }
}