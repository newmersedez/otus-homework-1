﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IRoleRepository : IRepository
{
    Task<IReadOnlyList<Role>> GetListAsync(IEnumerable<Guid> roleIds = null);
}