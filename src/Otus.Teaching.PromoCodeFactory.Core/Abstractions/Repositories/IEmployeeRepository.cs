﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IEmployeeRepository : IRepository
{
    Task<IReadOnlyList<Employee>> GetListAsync(IEnumerable<Guid> employeeIds = null);
    Task<Employee> GetAsync(Guid employeeId);
    void Add(Employee employee);
    void Remove(Employee employee);
}