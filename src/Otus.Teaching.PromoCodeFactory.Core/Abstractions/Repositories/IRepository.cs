﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IRepository
{
    Task SaveAsync();
}