﻿using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public abstract class Repository : IRepository
{
    protected readonly DatabaseContext Context;

    protected Repository(DatabaseContext context)
    {
        Context = context;
    }

    public async Task SaveAsync()
    {
        await Context.SaveChangesAsync();
    }
}