﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class RoleRepository : Repository, IRoleRepository 
{
    public RoleRepository(DatabaseContext context) 
        : base(context)
    { }

    public async Task<IReadOnlyList<Role>> GetListAsync(IEnumerable<Guid> roleIds = null)
    {
        var baseQuery = Context.Roles.AsQueryable();

        if (roleIds is not null)
        {
            baseQuery = baseQuery.Where(x => roleIds.Contains(x.Id));
        }

        return await baseQuery.ToListAsync();
    }
}