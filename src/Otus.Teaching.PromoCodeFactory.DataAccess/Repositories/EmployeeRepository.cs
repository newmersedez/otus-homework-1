﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EmployeeRepository : Repository, IEmployeeRepository
{
    public EmployeeRepository(DatabaseContext context) 
        : base(context)
    { }

    public async Task<IReadOnlyList<Employee>> GetListAsync(IEnumerable<Guid> employeeIds = null)
    {
        var baseQuery = Context.Employees
            .Include(x => x.Roles)
            .AsQueryable();

        if (employeeIds is not null)
        {
            baseQuery = baseQuery.Where(x => employeeIds.Contains(x.Id));
        }

        return await baseQuery.ToListAsync();
    }

    public async Task<Employee> GetAsync(Guid employeeId)
    {
        return await Context.Employees.Include(x => x.Roles).FirstOrDefaultAsync(x => x.Id == employeeId);
    }

    public void Add(Employee employee)
    {
        Context.Add(employee);
    }

    public void Remove(Employee employee)
    {
        Context.Remove(employee);
    }
}